# -*- coding:utf-8 -*-
import webob.exc
from nagare import presentation, log
from .comp import Perfs


@presentation.init_for(Perfs)
def init_for(self, url, comp, http_method, request):
    if url:
        try:
            app_index = [e[0] for e in self.widgets].index(url[0])
            self.select_nav(app_index)
            self.content.init(url[1:], http_method, request)
        except ValueError:
            log.info('%r: %s notfound', Perfs, '/'.join(url))
            raise webob.exc.HTTPNotFound('/'.join(url))
