# -*- coding:utf-8 -*-
import pkg_resources
import configobj
from collections import OrderedDict
from nagare import presentation, component, config

from nagare.contrib.jquery import wsgi

from nagare.contrib.ui.menu import Menu

from perf_graphs.services.plugins import Services


BOOTSTRAP_EXTERNAL_PREFIX = "https://cdnjs.cloudflare.com/ajax/libs/"
BOOTSTRAP_INTERNAL_PREFIX = ""

BOOTSTRAP_PREFIX = BOOTSTRAP_EXTERNAL_PREFIX

BOOTSTRAP_CSS = [
    'twitter-bootstrap/3.3.2/css/bootstrap-theme.min.css',
    'twitter-bootstrap/3.3.2/css/bootstrap.min.css'
]

BOOTSTRAP_JS = [
    'twitter-bootstrap/3.3.2/js/bootstrap.min.js'
]

APP_TITLE = 'Performance graphs'


class Perfs(object):

    """
    Performances application
    """

    def __init__(self, widgets, **kwargs):

        self.content = component.Component(None)

        self.widgets = widgets.items()

        self.navbar = component.Component(
            Menu(
                [k for k, v in self.widgets],
                ['nav', 'navbar-nav', 'navbar-right'],
                True
            )
        )

        self.navbar.on_answer(self.select_nav)

        self.select_nav(0)

    def select_nav(self, index):
        self.widgets[index][1].init()
        self.content.becomes(
            self.widgets[index][1], url=self.widgets[index][0])
        self.navbar().selected(index)


@presentation.render_for(Perfs)
def render_perfs(self, h, *args):
    """
    Design from http://getbootstrap.com/examples/dashboard/
    """

    h.head << h.head.meta(charset='utf-8')
    h.head << h.head.meta(
        **{'http-equiv': 'X-UA-Compatible', 'content': 'IE=edge'})
    h.head << h.head.meta(
        name='viewport', content='width=device-width, initial-scale=1')
    h.head << h.head.title(APP_TITLE)

    for css in BOOTSTRAP_CSS:
        h.head.css_url(BOOTSTRAP_PREFIX + css)

    h.head.css_url('css/dashboard.css')
    h.head.css_url('css/screen.css')

    with h.body:

        with h.div(class_='navbar navbar-inverse navbar-fixed-top',
                   role='navigation'):
            with h.div(class_='container-fluid'):
                with h.div(class_='navbar-header'):
                    h << h.a(APP_TITLE, href='#', class_='navbar-brand')

                with h.div(class_='collapse navbar-collapse'):
                    h << self.navbar

        with h.div(class_='container-fluid'):
            if self.content():
                h << self.content.render(h.AsyncRenderer())

        for js in BOOTSTRAP_JS:
            h.head.javascript_url(BOOTSTRAP_PREFIX + js)

        h.head.javascript_url('js/svg.jquery.js')
        h.head.javascript_url('js/pygal-tooltips.js')

    return h.root


@presentation.render_for(Perfs, model='list')
def render_perfs_list(self, h, comp, *args):
    return h.root


# ---------------------------------------------------------------
class WSGIApp(wsgi.WSGIApp):
    config_spec = {
        'application': {
            'widgets': 'list(default=list("charts"))',
            'services': 'list(default=list("storage"))'
        },
    }

    def __init__(self, root_factory):
        super(WSGIApp, self).__init__(root_factory)
        self.services = Services()
        self.widgets = OrderedDict()

    def _load_plugins(self, configuration_key, conf, spec, config_filename, error):

        all_plugins = {
            entry.name: entry.load()
            for entry in pkg_resources.iter_entry_points('perf_graphs.' + configuration_key)
        }

        for plugin in conf['application'][configuration_key]:
            plugin_conf = conf[configuration_key].get(plugin, {})

            spec.merge(
                {configuration_key: {
                    plugin: all_plugins[plugin_conf['type']].config_spec}}
            )
            conf = configobj.ConfigObj(conf, configspec=spec)
            config.validate(config_filename, conf, error)
            getattr(self, configuration_key)[plugin] = self.services(
                all_plugins[plugin_conf['type']])(**plugin_conf)

    def set_config(self, config_filename, conf, error):
        super(WSGIApp, self).set_config(config_filename, conf, error)

        spec = configobj.ConfigObj(WSGIApp.config_spec)
        conf = configobj.ConfigObj(conf, configspec=spec)
        config.validate(config_filename, conf, error)

        self._load_plugins('services', conf, spec, config_filename, error)
        self._load_plugins('widgets', conf, spec, config_filename, error)

    def create_root(self, *args, **kwargs):
        return self.services(super(WSGIApp, self).create_root)(self.widgets, *args, **kwargs)


app = WSGIApp(
    lambda *args, **kwargs: component.Component(Perfs(*args, **kwargs)))
