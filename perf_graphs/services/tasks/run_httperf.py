import logging
import os
from datetime import datetime
from tempfile import mkstemp
import csv
from httperfpy import Httperf
import paramiko
from paramiko import ssh_exception
from contextlib import closing
from socket import error as socket_error


def _run(server, port, uri, low_rate, high_rate, rate_step, num_conn, num_call, timeout, add_header):
    for rate in xrange(low_rate, high_rate + rate_step, rate_step):
        logging.info(
            "start test for %s:%d/%s @ rate %d", server, port, uri, rate)
        perf = Httperf(server=server, port=port, uri=uri, rate=rate,
                       num_conns=num_conn, num_calls=num_call, timeout=timeout,
                       add_header=add_header)
        perf.parser = True

        res = perf.run()

        logging.info("%s:%d/%s tested @ rate %d.", server, port, uri, rate)

        try:
            yield {
                'dem_req_rate': rate,
                'req_rate': res['request_rate_per_sec'],
                'con_rate': res['connection_rate_ms_conn'],
                'min_rep_rate': res['reply_rate_min'],
                'avg_rep_rate': res['reply_rate_avg'],
                'max_rep_rate': res['reply_rate_max'],
                'stddev_rep_rate': res['reply_rate_stddev'],
                'resp_time': res['reply_time_response'],
                'net_io': res['net_io_bps'],
                'errors': res['errors_total']
            }
        except KeyError:
            logging.error('Bad Httperf request %r', res)


def _create_csv(results, base_dir, name, version):
    name = '%s_%s_%s' % (name, version, datetime.now().strftime('%Y%m%d'))

    fd, temp_path = mkstemp()

    fields = [
        'dem_req_rate',
        'req_rate',
        'con_rate',
        'min_rep_rate',
        'avg_rep_rate',
        'max_rep_rate',
        'stddev_rep_rate',
        'resp_time',
        'net_io',
        'errors']
    with open(temp_path, 'w') as f:
        writer = csv.DictWriter(f, fields)
        writer.writeheader()
        writer.writerows(results)

    os.close(fd)
    logging.info(
        'Rename %s to %s', temp_path, os.path.join(base_dir, name + '.csv'))
    if os.path.exists(temp_path):
        os.rename(temp_path, os.path.join(base_dir, name + '.csv'))
    else:
        logging.error('Missing temporary file %s', temp_path)


def _get_version(server, username, current_path):
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(server, username=username, look_for_keys=True)

    with closing(ssh_client) as ssh:
        stdin, stdout, stderr = ssh.exec_command('readlink %s' % current_path)
        version = stdout.read().strip().split('/')[-1:]
        return version


def run(server, port, uri, low_rate, high_rate, rate_step, num_conn, num_call, timeout, add_header, base_dir, name,
        ssh_server, ssh_login, ssh_path):

    try:
        version = _get_version(ssh_server, ssh_login, ssh_path)
    except (ssh_exception.BadHostKeyException, ssh_exception.AuthenticationException, ssh_exception.SSHException, socket_error):
        logging.exception('Version error')
        version = datetime.now().strftime("%Y%m%d:%H%M%S")

    return _create_csv(
        _run(server, port, uri, low_rate, high_rate, rate_step,
             num_conn, num_call, timeout, add_header),
        base_dir,
        name,
        version)
