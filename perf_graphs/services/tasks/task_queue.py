import pickle

import zmq
import cloud
import logging

# TODO: Move this into a configuration file
HOST = '127.0.0.1'
PORT = 9090
TASK_SOCKET = zmq.Context().socket(zmq.PUB)
SOCKET_URI = 'ipc:///tmp/perf_graphs_tasks.ipc'
TASK_SOCKET.connect(SOCKET_URI)


class Worker(object):

    """A remote task executor."""

    def __init__(self, host=HOST, port=PORT):
        """Initialize worker."""
        self.host = host
        self.port = port
        self._context = zmq.Context()
        self._socket = self._context.socket(zmq.SUB)
        self._socket.setsockopt(zmq.SUBSCRIBE, '')

    def start(self):
        """Start listening for tasks."""
        self._socket.bind(SOCKET_URI)
        while True:
            msg = self._socket.recv_pyobj()
            runnable = pickle.loads(msg['runnable'])
            response = self._do_work(runnable, msg['args'], msg['kwargs'])

    def _do_work(self, task, args, kwargs):
        """Return the result of executing the given task."""
        print('Running [{}] with args [{}] and kwargs [{}]'.format(
            task, args, kwargs))
        try:
            return task(*args, **kwargs)
        except:
            logging.exception('Error running task')
            return

    @classmethod
    def run(cls):
        logging.basicConfig(level=logging.INFO)
        cls().start()


def queue(runnable, *args, **kwargs):
    """Return the result of running the task *runnable* with the given
    arguments."""
    runnable_string = cloud.serialization.cloudpickle.dumps(runnable)
    TASK_SOCKET.send_pyobj(
        {'runnable': runnable_string, 'args': args, 'kwargs': kwargs})

if __name__ == '__main__':
    Worker.run()
