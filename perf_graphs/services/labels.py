# -*- coding:utf-8 -*-
TRANS_DICT = {
    'menu': {
        'ab_low': "ab low",
        'ab_low_pong': "ab low (pong)",
        'ab_high': "ab high",
        'ab_high_pong': "ab high (pong)",
        'httperf_low': "httperf low",
        'httperf_low_pong': "httperf low (pong)",
        'httperf_high': "httperf high",
        'httperf_high_pong': "httperf high (pong)"
    },
    'main': {
        'ab_low': "ab low concurrency level (40 - 1000)",
        'ab_low_pong': "ab low concurrency level (40 - 1000) "
                       "for pong application",
        'ab_high': "ab high concurrency level (400 - 9000)",
        'ab_high_pong': "ab high concurrency level (400 - 9000) "
                        "for pong application",
        'httperf_low': "httperf low request rate (40 - 1000)",
        'httperf_low_pong': "httperf low request rate (40 - 1000) "
                            "for pong application",
        'httperf_high': "httperf high request rate (400 - 9000)",
        'httperf_high_pong': "httperf high request rate (400 - 9000) "
                             "for pong application",
        'errors': 'Number of errors',
        'stddev_rep_rate': 'Standard deviation for response rate',
        'avg_rep_rate': 'Average response rate',
        'max_rep_rate': 'Maximum response rate',
        'min_rep_rate': 'Minimum response rate',
        'resp_time': 'Response time',
        'con_rate': 'Connection rate',
        'req_rate': 'Request rate',
        'net_io': 'Data transfered',
        'concurrence_nagare_admin': 'Nagare Admin (concurrence)',
        'concurrence_nagare_pong': 'Nagare Pong (concurrence)',
        'concurrence2_nagare_admin': 'Nagare Admin (concurrence2)',
        'concurrence2_nagare_pong': 'Nagare Pong (concurrence2)',
        'syncless_nagare_admin': 'Nagare Admin (syncless)',
        'syncless_nagare_pong': 'Nagare Pong (syncless)',
        'concurrence_pong': 'Pong WSGI (concurrence)',
        'syncless_pong': 'Pong WSGI (syncless)',
        'fastcgi_nagare_pong': 'Nagare Pong (Apache + mod_fastcgi)',
    },
}


def _tr(in_str, context='main'):
    return TRANS_DICT.get(context, {}).get(in_str, in_str)
