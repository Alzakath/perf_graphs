# -*- coding:utf-8 -*-
import configobj
from validate import Validator
from nagare import log


class ConfigParser(object):
    conf_spec = {
        'host1': 'string(default=)',
        'uri1': 'string(default=)',
        'port1': 'integer(default=80)',
        'low_rate': 'integer(default=10)',
        'high_rate': 'integer(default=70)',
        'rate_step': 'integer(default=10)',
        'num_conn': 'integer(default=2100)',
        'num_call': 'integer(default=1)',
        'timeout': 'integer(default=5)',
        'output_fmt': 'string(default=csv)',
        'failure_status': 'string(default="")',
        'httperf_add-header': 'string(default="")',
        'ssh_login': 'string(default="")',
        'ssh_server': 'string(default=None)',
        'ssh_path': 'string(default="")',
    }

    def __init__(self, configuration_file):
        self.configuration_file = configuration_file

    @property
    def spec(self):
        return configobj.ConfigObj(self.conf_spec)

    def parse(self):
        conf = configobj.ConfigObj(self.configuration_file,
                                   configspec=self.spec,
                                   interpolation='Template')

        return conf

    def write(self, conf):
        conf.write()

    def validate(self, conf):
        errors = configobj.flatten_errors(conf,
                                          conf.validate(Validator(),
                                                        preserve_errors=True))

        for (sections, name, error) in errors:
            log.warning('file "%s", section "[%s]", parameter "%s": %s',
                        self.configuration_file, '/'.join(sections), name, error)

        return errors
