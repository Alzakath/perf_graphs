# -*- coding:utf-8 -*-
import os
from perf_graphs.services.tasks import run_httperf, task_queue

from .plugins import Service


class Application(object):

    def __init__(self, app_conf):
        self.app_conf = app_conf
        self.name = app_conf.name

    def run_test(self):
        task_queue.queue(run_httperf.run, self.app_conf.server, self.app_conf.port, self.app_conf.uri,
                         self.app_conf.low_rate, self.app_conf.high_rate,
                         self.app_conf.rate_step, self.app_conf.num_conn, self.app_conf.num_call, self.app_conf.timeout,
                         self.app_conf.add_header, self.app_conf.storage_app.base_dir, self.app_conf.name,
                         self.app_conf.ssh_server, self.app_conf.ssh_login, self.app_conf.ssh_path)


class HTTPerfTestRunner(Service):

    def __init__(self, test_storage_service, test_suite_service, **kwargs):
        self.test_suite = test_suite_service
        self.storage_service = test_storage_service

    def get_applications(self):
        for app in self.test_suite.get_applications():
            yield Application(app)

    def get_application(self, application_name):
        return Application(self.test_suite.get_application(application_name))
