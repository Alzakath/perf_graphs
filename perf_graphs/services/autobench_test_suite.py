import os
from perf_graphs.services.autobench_config_parser import ConfigParser
from .plugins import Service


class Application(object):

    def __init__(self, name, base_dir, test_storage_service, **kwargs):
        self.name = name
        self.configuration_file = os.path.join(
            base_dir, 'autobench_%s.conf' % self.name)
        self.server = ''
        self.uri = ''
        self.port = 80
        self.low_rate = 10
        self.high_rate = 70
        self.rate_step = 10
        self.num_conn = 2100
        self.num_call = 1
        self.timeout = 5
        self.output_fmt = 'csv'
        self.add_header = ''
        self.failure_status = ''
        self.storage_app = test_storage_service.get_application(name)
        self._config_parser = ConfigParser(self.configuration_file)

    def load(self):
        conf = self._config_parser.parse()

        self._config_parser.validate(conf)

        self.server = conf['host1']
        self.uri = conf['uri1']
        self.port = conf['port1']
        self.low_rate = conf['low_rate']
        self.high_rate = conf['high_rate']
        self.rate_step = conf['rate_step']
        self.num_conn = conf['num_conn']
        self.num_call = conf['num_call']
        self.timeout = conf['timeout']
        self.output_fmt = conf['output_fmt']
        self.add_header = conf.get('httperf_add-header')
        self.failure_status = conf.get('failure_status')
        if conf['ssh_server']:
            self.ssh_server = conf['ssh_server']
        else:
            self.ssh_server = self.server
        self.ssh_login = conf['ssh_login']
        self.ssh_path = conf['ssh_path']
        return self

    def save(self):
        conf = self._config_parser.parse()

        conf['host1'] = self.server
        conf['uri1'] = self.uri
        conf['port1'] = self.port
        conf['low_rate'] = self.low_rate
        conf['high_rate'] = self.high_rate
        conf['rate_step'] = self.rate_step
        conf['num_conn'] = self.num_conn
        conf['num_call'] = self.num_call
        conf['timeout'] = self.timeout
        conf['output_fmt'] = self.output_fmt
        conf['httperf_add-header'] = self.add_header
        conf['failure_status'] = self.failure_status

        errors = self._config_parser.validate(conf)

        if not errors:
            self._config_parser.write(conf)
            return True
        else:
            return False


class AutobenchTestSuite(Service):

    config_spec = {'base_dir': 'string(default="")'}

    def __init__(self, services_service, base_dir='', **kwargs):

        self.base_dir = base_dir
        self.services = services_service

    def get_applications(self):
        for e in os.listdir(self.base_dir):
            app, ext = os.path.splitext(e)
            if ext == '.conf':

                yield self.get_application(app.replace('autobench_', '')).load()

    def get_application(self, application_name):
        return self.services(Application)(application_name, self.base_dir)
