from collections import OrderedDict
import inspect
from functools import partial


class PlugIn(object):
    config_spec = {}


class Service(PlugIn):

    """
    """


class Widget(PlugIn):

    """
    """


class Services(OrderedDict):

    """

    """

    def inject_services(self, _callable, *args, **kwargs):
        _callable = _callable.__init__ if isinstance(
            _callable, type) else _callable
        arg_spec = inspect.getargspec(_callable)

        nb_defaults = len(arg_spec.defaults) if arg_spec.defaults else 0
        if nb_defaults:
            keywords = dict(
                zip(arg_spec.args[-nb_defaults:], arg_spec.defaults))
        else:
            keywords = {}

        keywords.update(
            {name + '_service': service for name, service in self.iteritems()})
        keywords['services_service'] = self

        return keywords

    def __call__(self, _callable, *args, **kwargs):
        kwargs = self.inject_services(_callable)
        return partial(_callable, *args, **kwargs)
