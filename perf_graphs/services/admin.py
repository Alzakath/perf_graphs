# -*- coding:utf-8 -*-
from nagare import presentation, component, validator, log
from nagare.i18n import _
from nagare.editor import Editor
from nagare.contrib.ui import paginator
from nagare.contrib.ui.menu import Menu

from .plugins import Widget


class Admin(Widget):

    def __init__(self, test_storage_service, test_runner_service, **kwargs):
        self.test_storage = test_storage_service
        self.test_runner = test_runner_service

        self.actions = [(_('Filter apps'), AdminFilterApps),
                        (_('Configure apps'), AdminAppList)]

        self.content = component.Component(None)

        self.actions_menu = component.Component(None)
        self.actions_menu.on_answer(self.select_action)

    def get_action_label(self):
        return self.actions[self.actions_menu().selected()][0]

    def select_action(self, index):
        self.content.becomes(
            self.actions[index][1](self.test_storage, self.test_runner))
        self.actions_menu().selected(index)

    def init(self):
        self.actions_menu.becomes(
            Menu([e[0] for e in self.actions], ['dropdown-menu', ], True))
        self.select_action(0)


class AdminAppList(object):

    def __init__(self, test_storage, test_runner):
        self.test_runner = test_runner
        self._applications = list(self.test_runner.get_applications())
        self.paginator = component.Component(None)
        self.paginator.on_answer(self.select_page)
        self.slice_size = 10
        self.slice_start = 0
        if len(self._applications) > self.slice_size:
            self.paginator.becomes(paginator.PaginatorMenu(1, self.slice_size,
                                                           len(
                                                               self._applications)))
            self.select_page(1)
        else:
            self.paginator.becomes(None)

    @property
    def applications(self):
        return self._applications[
            self.slice_start:self.slice_start + self.slice_size]

    def select_page(self, index):
        self.slice_start = (index - 1) * self.slice_size
        self.paginator().page(index)

    def call_app(self, app, comp):
        comp.call(AdminAppConf(app), model='admin')


class AdminFilterApps(object):

    def __init__(self, test_storage, test_runner):
        self.test_storage = test_storage
        self._applications = list(
            self.test_storage.get_applications(filter=False))
        self.paginator = component.Component(None)
        self.paginator.on_answer(self.select_page)
        self.slice_size = 10
        self.slice_start = 0
        if len(self._applications) > self.slice_size:
            self.paginator.becomes(paginator.PaginatorMenu(1, self.slice_size,
                                                           len(
                                                               self._applications)))
            self.select_page(1)
        else:
            self.paginator.becomes(None)

    @property
    def applications(self):
        return self._applications[
            self.slice_start:self.slice_start + self.slice_size]

    def select_page(self, index):
        self.slice_start = (index - 1) * self.slice_size
        self.paginator().page(index)

    def call_app(self, app, comp):
        comp.call(AdminApp(app), model='admin')


class AdminApp(object):

    def __init__(self, app):
        self.app = app
        self._test_results = list(self.app.get_test_results())
        self.slice_size = 10
        self.slice_start = 0
        self.paginator = component.Component(None)
        self.paginator.on_answer(self.select_page)

        if len(self._test_results) > self.slice_size:
            self.paginator.becomes(paginator.PaginatorMenu(1, self.slice_size,
                                                           len(
                                                               self._test_results)))
            self.select_page(1)
        else:
            self.paginator.becomes(None)

    def select_page(self, index):
        self.slice_start = (index - 1) * self.slice_size
        self.paginator().page(index)

    @property
    def test_results(self):
        return self._test_results[
            self.slice_start:self.slice_start + self.slice_size]

    def toggle_test_disabled(self, test):
        self.app.toggle_test_disabled(test)
        self._test_results = list(self.app.get_test_results())


class AdminAppConf(Editor):
    keys = [
        'server',
        'uri',
        'port',
        'low_rate',
        'high_rate',
        'rate_step',
        'num_conn',
        'num_call',
        'timeout',
        'add_header'
    ]

    def __init__(self, app):
        super(AdminAppConf, self).__init__(app.app_conf, self.keys)
        self.server.validate(
            lambda name: validator.StringValidator(name).not_empty()
        )

        self.port.validate(
            lambda name: validator.IntValidator(
                name).lesser_or_equal_than(65535).greater_or_equal_than(1)
        )

        self.low_rate.validate(
            lambda name: validator.IntValidator(name).greater_or_equal_than(1)
        )

        self.high_rate.validate(
            lambda name: validator.IntValidator(name).greater_or_equal_than(1)
        )

        self.rate_step.validate(
            lambda name: validator.IntValidator(name).greater_or_equal_than(1)
        )

        self.num_conn.validate(
            lambda name: validator.IntValidator(name).greater_or_equal_than(1)
        )

        self.num_call.validate(
            lambda name: validator.IntValidator(name).greater_or_equal_than(1)
        )

        self.timeout.validate(
            lambda name: validator.IntValidator(name).greater_or_equal_than(1)
        )
        self.app = app

    def submit(self, comp):

        if self.commit(self.keys):
            if self.target.save():
                comp.answer()

    def run_test(self):
        log.info('start run_test')
        if self.commit(self.keys):
            log.info('run_test commit OK')
            if self.target.save():
                log.info('run_test save OK')
                self.app.run_test()
                log.info('run_test test ran')
        log.info('end run_test')


@presentation.render_for(AdminAppConf, model='admin')
def render_admin(self, h, comp, *args):
    with h.div:
        h << h.h2(self.target.name)
        with h.form(role='form'):
            with h.fieldset:
                with h.div(class_='form-group'):
                    h << h.label(_('Server'))
                    h << h.input(
                        type='text',
                        class_='form-control',
                        placeholder=_('Enter server address'),
                        value=self.server()
                    ).action(self.server).error(self.server.error)

                with h.div(class_='form-group'):
                    h << h.label(_('Port'))
                    h << h.input(
                        type='number',
                        class_='form-control',
                        placeholder=_('Enter port'),
                        value=self.port()
                    ).action(self.port).error(self.port.error)

                with h.div(class_='form-group'):
                    h << h.label(_('Uri'))
                    h << h.input(
                        type='text',
                        class_='form-control',
                        placeholder=_('Enter page uri'),
                        value=self.uri()
                    ).action(self.uri).error(self.uri.error)

            with h.fieldset:
                cls = ['form-group']
                if self.low_rate.error:
                    cls.extend(['has-error', 'has-feedback'])

                with h.div(class_=' '.join(cls)):
                    h << h.label(_('Low rate'))
                    h << h.input(
                        type='number',
                        class_='form-control',
                        placeholder=_('Enter low rate'),
                        value=self.low_rate()
                    ).action(self.low_rate)
                    if self.low_rate.error:
                        h << h.p(self.low_rate.error, class_='bg-danger')

                with h.div(class_='form-group'):
                    h << h.label(_('High rate'))
                    h << h.input(
                        type='number',
                        class_='form-control',
                        placeholder=_('Enter high rate'),
                        value=self.high_rate()
                    ).action(self.high_rate).error(self.high_rate.error)

                with h.div(class_='form-group'):
                    h << h.label(_('Rate step'))
                    h << h.input(
                        type='number',
                        class_='form-control',
                        placeholder=_('Enter rate step'),
                        value=self.rate_step()
                    ).action(self.rate_step).error(self.rate_step.error)

                with h.div(class_='form-group'):
                    h << h.label(_('Number of connexions'))
                    h << h.input(
                        type='number',
                        class_='form-control',
                        placeholder=_('Enter number of connexions'),
                        value=self.num_conn()
                    ).action(self.num_conn).error(self.num_conn.error)

                with h.div(class_='form-group'):
                    h << h.label(_('Number of calls per connexion'))
                    h << h.input(
                        type='number',
                        class_='form-control',
                        placeholder=_('Enter number of calls'),
                        value=self.num_call()
                    ).action(self.num_call).error(self.num_call.error)

                with h.div(class_='form-group'):
                    h << h.label(_('Timeout'))
                    h << h.input(
                        type='number',
                        class_='form-control',
                        placeholder=_('Enter timeout'),
                        value=self.timeout()
                    ).action(self.timeout).error(self.timeout.error)

                with h.div(class_='form-group'):
                    h << h.label(_('Headers'))
                    h << h.input(
                        type='text',
                        class_='form-control',
                        placeholder=_('Add headers'),
                        value=self.add_header()
                    ).action(self.add_header).error(self.add_header.error)

            with h.div(class_='form-group'):
                h << h.input(
                    type='submit', class_='btn btn-default', value=_('Cancel')).action(comp.answer)
                h << h.input(type='submit', class_='btn btn-warning',
                             value=_('Launch test')).action(self.run_test)
                h << h.input(type='submit', class_='btn btn-primary',
                             value=_('Validate')).action(self.submit, comp)
    return h.root


@presentation.render_for(AdminFilterApps)
def render_admin(self, h, comp, *args):
    with h.table(class_='table'):
        with h.tr:
            h << h.th('Application name')
            h << h.th('Disabled')
        for app in self.applications:
            with h.tr:
                h << h.td(h.a(app.name).action(self.call_app, app, comp))
                h << h.td(h.a('Y' if app.disabled else 'N').action(
                    app.toggle_disabled))

    if self.paginator():
        with h.div(style='text-align:center'):
            h << self.paginator
    return h.root


@presentation.render_for(AdminAppList)
def render_admin(self, h, comp, *args):
    with h.table(class_='table'):
        with h.tr:
            h << h.th('Application name')
        for app in self.applications:
            with h.tr:
                h << h.td(h.a(app.name).action(self.call_app, app, comp))

    if self.paginator():
        with h.div(style='text-align:center'):
            h << self.paginator
    return h.root


@presentation.render_for(Admin)
def render_admin(self, h, comp, *args):
    with h.div(class_='row'):
        with h.div(class_='col-sm-3 col-md-2 sidebar'):
            with h.ul(class_='nav nav-pills'):
                with h.li(class_='dropdown'):
                    with h.a(_('Change actions'),
                             **{'data-toggle': 'dropdown', 'href': '#'}):
                        h << self.actions_menu

        with h.div(class_='col-sm-9 col-sm-offset-3 '
                          'col-md-10 col-md-offset-2 main'):
            h << h.h1(self.get_action_label())
            if self.content():
                h << self.content
    return h.root


@presentation.render_for(AdminApp, model='admin')
def render_application_admin(self, h, comp, *args):
    with h.table(class_='table'):
        with h.tr:
            h << h.th('Test name')
            h << h.th('Disabled')
        for test, disabled in self.test_results:
            with h.tr:
                h << h.td(test)
                h << h.td(h.a('Y' if disabled else 'N').action(
                    self.toggle_test_disabled, test))

    if self.paginator():
        with h.div(style='text-align:center'):
            h << self.paginator

    h << h.a('<< ', _('Back')).action(comp.answer)

    return h.root


@presentation.init_for(Admin)
def init_for_admin(self, url, comp, http_method, request):
    pass
