# -*- coding:utf-8 -*-
import os
import webob.exc
from nagare import presentation, component, log
from nagare.i18n import _

from nagare.contrib.ui.menu import Menu

from .labels import _tr
from .svg import svg_from_series
from .plugins import Widget


class Empty(object):

    def __init__(self, msg=None):
        self.msg = msg


@presentation.render_for(Empty)
def render_empty(self, h, comp, *args):
    if self.msg is not None:
        h << self.msg
    return h.root


class Chart(object):

    """
    HTTPerf chart component
    """

    def __init__(self, graph_name, graph_data):
        self.graph_name = graph_name
        self.graph_data = graph_data


class Charts(Widget):

    """
    Component that handle the list of charts
    """

    def __init__(self, test_storage_service, **kwargs):
        self.test_storage = test_storage_service
        self.applications = []
        self.content = component.Component(None)

        self.applications_menu = component.Component(None)
        self.applications_menu.on_answer(self.select_app)

        self.init()

    def select_app(self, index):
        self.content.becomes(
            ChartList(self.test_storage, self.applications[index]),
            url=self.applications[index]
        )
        self.applications_menu().selected(index)

    def init(self):
        self.applications = [
            app.name for app in self.test_storage.get_applications()]
        self.applications_menu.becomes(
            Menu(self.applications, ['dropdown-menu', ], True))
        self.select_app(0)


@presentation.render_for(Charts)
def render_charts(self, h, comp, *args):
    with h.div(class_='row'):
        with h.div(class_='col-sm-3 col-md-2 sidebar'):
            with h.ul(class_='nav nav-pills'):
                with h.li(class_='dropdown'):
                    with h.a(_('Change application'),
                             **{'data-toggle': 'dropdown', 'href': '#'}):
                        h << self.applications_menu

        with h.div(class_='col-sm-9 col-sm-offset-3 '
                          'col-md-10 col-md-offset-2 main'):
            if self.content():
                h << self.content

    return h.root


class ChartList(object):

    """
    Component that handle the list of charts
    """

    def __init__(self, test_storage, application_name):
        # self.folders = folders
        self.test_storage = test_storage
        self.application_name = application_name
        # self.graphs = self.parse_csv_files()
        self.current_chart = component.Component(None)
        self.menu = component.Component(
            Menu(self.graphs.keys(), classes=['nav', 'nav-tabs']))
        self.menu.on_answer(self.activate_graph)
        if len(self.graphs.keys()) > 1:
            self.activate_graph(0)
        else:
            self.current_chart.becomes(Empty())

    @property
    def application(self):
        return self.test_storage.get_application(self.application_name)

    @property
    def graphs(self):
        return self.application.get_graphs()

    def activate_graph(self, index):
        key = self.graphs.keys()[index]
        series = self.graphs.get(key, {'rates': {}, 'servers': []})
        self.current_chart.becomes(Chart(key, series), url=key)

        self.menu().selected(index)


@presentation.render_for(Chart)
def render_chart(self, h, comp, *args):
    chart_id = h.generate_id('chart')

    with h.div(id=chart_id, class_='ui-perf-chart'):

        h << h.h3(_tr(self.graph_name))

        with h.div(class_="chart_container"):
            h << h.object(type_="image/svg+xml", data=h.url + '.svg',
                          class_='svg-graph')

        kwargs = {'class': 'table-collapse',
                  'data-toggle': 'collapse',
                  'data-parent': '#%s' % chart_id,
                  'data-target': '#%s .table-responsive' % chart_id}

        h << h.h3(h.a(_('Raw data'), class_='table-collapse', **kwargs))

        with h.div(class_='table-responsive collapse'):
            with h.table(class_="table"):

                sorted_servers = list(sorted(self.graph_data['servers']))
                with h.thead:
                    with h.tr:
                        h << h.td('Demanded request rate', alt='rate')
                        for k in sorted_servers:
                            h << h.td(_tr(k), alt=k)

                with h.tbody:
                    for rate, values in sorted(
                            self.graph_data['rates'].iteritems()):
                        with h.tr:
                            h << h.td(rate)
                            for i in sorted_servers:
                                with h.td:
                                    ind = self.graph_data['servers'].index(i)
                                    try:
                                        h << (values[ind] or '')
                                    except:
                                        log.warning('Bad Value', exc_info=True)
                                        h << 0.0
    return h.root


@presentation.render_for(ChartList)
def render_chart_list(self, h, comp, *args):
    h << h.h1(self.application_name, class_='page-header')
    with h.div(class_='row placeholders'):
        with h.div(class_='col-md-12'):
            h << self.menu

    h << self.current_chart

    return h.root


@presentation.init_for(Charts)
def init_for_charts(self, url, comp, http_method, request):
    if url:
        try:
            i = self.applications.index(url[0])
            self.select_app(i)
            if len(url) > 1:
                self.content.init(url[1:], http_method, request)
        except ValueError:
            log.info('%r: %s notfound', Charts, '/'.join(url))
            raise webob.exc.HTTPNotFound('/'.join(url))


@presentation.init_for(ChartList)
def init_for_chart_list(self, url, comp, http_method, request):
    if url:
        key, ext = os.path.splitext(url[0])
        if key in self.graphs.keys():
            series = self.graphs.get(key, {'rates': {}, 'servers': []})
            self.current_chart.becomes(Chart(key, series), url=key)
            self.current_chart.init(url, http_method, request)
        else:
            log.info('%r: %s notfound', ChartList, '/'.join(url))
            raise webob.exc.HTTPNotFound('/'.join(url))


@presentation.init_for(Chart)
def init_for_chart(self, url, comp, http_method, request):
    if url:
        key, ext = os.path.splitext(url[0])
        if ext:
            if ext == '.svg':
                res = svg_from_series(self.graph_data, _tr(self.graph_name))
                e = webob.exc.HTTPOk()
                e.content_type = 'image/svg+xml'
                e.body = res
                raise e
            else:
                log.info('%r: %s notfound', Chart, '/'.join(url))
                raise webob.exc.HTTPNotFound('/'.join(url))
