# -*- coding:utf-8 -*-
import pygal


def svg_from_series(series, title=''):
    line_chart = pygal.Line(
        legend_box_size=10, legend_font_size=12, legend_at_bottom=True)
    line_chart.title = title
    line_chart.x_labels = sorted(series['rates'].keys())

    for i, server in enumerate(series['servers']):
        line_chart.add(server, [float(series['rates'][x][i]) if series[
                       'rates'][x][i] else None for x in line_chart.x_labels])

    return line_chart.render()
