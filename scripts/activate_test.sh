#!/bin/bash

case $1 in
    webspace) ln -fs /home/protest/projects/perf_graphs/scripts/autobench_webspace-int.conf ~/.autobench.conf;;
    selfcare) ln -fs /home/protest/projects/perf_graphs/scripts/autobench_selfcare-int.conf ~/.autobench.conf;;
    damcache) ln -fs /home/protest/projects/perf_graphs/scripts/autobench_damcache-int.conf ~/.autobench.conf;;
    socollect) ln -fs /home/protest/projects/perf_graphs/scripts/autobench_socollect-int.conf ~/.autobench.conf;;
    socollect-db) ln -fs /home/protest/projects/perf_graphs/scripts/autobench_socollect-int-db.conf ~/.autobench.conf;;
    sowebspace) ln -fs /home/protest/projects/perf_graphs/scripts/autobench_sowebspace-int.conf ~/.autobench.conf;;
    webspacebosite) ln -fs /home/protest/projects/perf_graphs/scripts/autobench_webspace-bosite-int.conf ~/.autobench.conf;;
    boprosp) ln -fs /home/protest/projects/perf_graphs/scripts/autobench_boprosp.conf ~/.autobench.conf;;
    gyoji) ln -fs /home/protest/projects/perf_graphs/scripts/autobench_gyoji.conf ~/.autobench.conf;;
    risi) ln -fs /home/protest/projects/perf_graphs/scripts/autobench_risi.conf ~/.autobench.conf;;
    ar_rhm) ln -fs /home/protest/projects/perf_graphs/scripts/autobench_ar-rhm.conf ~/.autobench.conf;;
    *) echo "no configuration file for $1";;
esac

