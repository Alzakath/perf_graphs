# -*- coding:utf-8 -*-
import argparse
from perf_graphs.services.tasks import run_httperf
global apps


def parse_args():
    parser = argparse.ArgumentParser(description='Run Httperf tests.')
    parser.add_argument('application_name', metavar='APPLICATION', nargs=1,
                        help='application name')

    args = parser.parse_args()

    return args.application_name[0]


configuration_name = parse_args()
app_test_runner = apps['perf_graphs'].test_runner.get_application(
    configuration_name).load()
run_httperf.run(
    app_test_runner.server,
    app_test_runner.port,
    app_test_runner.uri,
    app_test_runner.low_rate,
    app_test_runner.high_rate,
    app_test_runner.rate_step,
    app_test_runner.num_conn,
    app_test_runner.num_call,
    app_test_runner.timeout,
    app_test_runner.add_header,
    app_test_runner.storage_app.base_dir,
    app_test_runner.name)
