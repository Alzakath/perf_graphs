#!/bin/bash

here=`dirname $0`
cd $here

#selfcare
#webspace
#damcache
#socollect
#socollect-db
#sowebspace
#webspacebosite
#boprosp
#gyoji

app_list="gyoji"

checkversion() {
    case "$1" in
        selfcare)
        echo `ssh proexp@selfcare-int 'readlink /exec/applis/j7acar/current' | cut -f 5 -d"/" -`
        ;;
        webspace)
        echo `ssh proexp@webspace-int 'readlink /exec/applis/jz110/current' | cut -f 5 -d"/" -`
        ;;
        damcache)
        echo `ssh proexp@damcache-int 'readlink /exec/applis/j7adam/current' | cut -f 5 -d"/" -`
        ;;
        socollect)
        echo `ssh pjmsexp@socollect-int 'readlink /exec/pjmspviselfcare/current' | cut -f 4 -d"/" -`
        ;;
        socollect-db)
        echo `ssh pjmsexp@socollect-int 'readlink /exec/pjmspvidb/current' | cut -f 4 -d"/" -`
        ;;
        sowebspace)
        echo `ssh pjmsexp@sowebspace-int 'readlink /exec/pjmspviwebspace/current' | cut -f 4 -d"/" -`
        ;;
        webspacebosite)
        echo `ssh proexp@192.168.100.79 'readlink /home/proexp/pj-bosite-preview/exec/current' | cut -f 6 -d"/" -`
        ;;
        boprosp)
        echo `ssh proexp@192.168.100.100 'readlink /exec/applis/j7abop/current' | cut -f 6 -d"/" -`
        ;;
        gyoji)
        echo `ssh inventiv@inso-int 'readlink /home/inventiv/projects/montaneus.com/current' | cut -f 6 -d"/" -`
        ;;
        risi)
        echo `ssh rinterim@192.168.100.163 'readlink /home/rinterim/projects/ri_si_ci/current' | cut -f 6 -d"/" -`
        ;;
        *)
    esac
}

for app_name in $app_list
do
    echo "Test application app: $app_name"
    $here/activate_test.sh $app_name
    version=`checkversion $app_name | sed 's#\.#_#g'`
    date=`date +'%Y%m%d'`
    echo "Start test of $app_name ($version) on $date"
    $here/autobench.sh ../data/raw_csv/$app_name/${app_name}_${version}_${date}.csv
    echo "$app_name test" 
done

