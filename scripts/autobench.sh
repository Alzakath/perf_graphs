#!/bin/bash

#Script used to launch httperf
#A modified version of httperf is added in PATH
#autobench uses .autobench.conf you need to replace it with the right
#one for your test
export PATH=/usr/local/bin:$PATH

/usr/local/bin/autobench --single_host --file $1
