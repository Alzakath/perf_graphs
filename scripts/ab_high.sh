#!/bin/bash

#Script used to launch apachebench
#and to generate csv files in the same way autobench does

COMMAND=/usr/bin/ab
SERVER_IP="192.168.100.198"
#SERVER_IP="nagare-test.net-ng.com"
SERVER_PORT="8080"
#SERVER_PORT="8080"
SERVER_URI="/admin/"
#SERVER_URI="/nagare_pong/"
RATE="00"
TOTAL_REQ=40000
LOW_RATE=4
HIGH_RATE=90

echo "dem_req_rate,req_rate_nagare-server,resp_time_nagare-server,net_io_nagare-server,errors_nagare-server"

echo "" > /tmp/result.txt

for (( i = $LOW_RATE; i <= $HIGH_RATE; i++ ))
do

RES=$($COMMAND -n $TOTAL_REQ  -c ${i}${RATE} http://${SERVER_IP}:${SERVER_PORT}${SERVER_URI} 2>/dev/null);
code=$?

dem_req_rate=${i}${RATE}
req_rate=$(echo $RES|sed -e "s/.*Requests per second: \([0-9\.]*\) \[.*/\1/g")
resp_time=$(echo $RES|sed -e "s/.*Time per request: \([0-9\.]*\) \[.*/\1/g")
net_io=$(echo $RES|sed -e "s/.*Transfer rate: \([0-9\.]*\) \[.*/\1/g")
errors=$(echo $RES|sed -e "s/.*Failed requests: \([0-9]*\) .*/\1/g")

echo $RES >> /tmp/result.txt

if [ $code -ne 0 ]; then
    echo "$dem_req_rate,0,0,0,$TOTAL_REQ"
else
    echo "$dem_req_rate,$req_rate,$resp_time,$net_io,$errors"
fi

done