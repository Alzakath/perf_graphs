#!/bin/bash

#Script used to launch httperf with rate from 400 to 9000 by 100
#Replace by autobench only kept for informational purpose on httperf use

#Here we use a modified httperf version (change limit to number of opened descriptors cf. README)

HTTPERF=$HOME/.local/httperf/bin/httperf
SERVER_IP="192.168.100.198"
SERVER_PORT="8080"
SERVER_URI="/admin/"
RATE="00"

OUTDIR=$(date +"/tmp/httperf_out_%Y%m%d_%H.%M")

mkdir -p $OUTDIR

for (( i = 4; i <= 90; i++ ))
do

$HTTPERF --hog --timeout=5 --client=0/1 --server=$SERVER_IP --port=$SERVER_PORT --uri=$SERVER_URI --rate=$i$RATE --send-buffer=4096 --recv-buffer=16384 --num-conns=40000 --num-calls=1 > $OUTDIR/httperf_out_$i.txt

done
