HTTPerf installation
--------------------

1) Edit /etc/security/limits.conf and add the line * hard nofile 65535
(or instead of * you can put the username of the user for whom you want to change the limit)

2) Edit /usr/include/bits/typesizes.h and change
#define __FD_SET_SIZE 1024 to #define __FD_SET_SIZE 65535
(in /usr/include/sys/select.h FD_SETSIZE is defined as __FD_SETSIZE)

3) Download and recompile httperf

Environment tweaking
--------------------

1) Add the following to /etc/sysctl.conf

#httperf customization
net.core.netdev_max_backlog = 2500
net.core.somaxconn = 250000
fs.file-max = 128000
net.ipv4.tcp_keepalive_time = 300
net.core.somaxconn = 250000
net.ipv4.tcp_max_syn_backlog = 2500
net.ipv4.ip_local_port_range = 10152 65535

2) change ulimit

ulimit -n 10240

(you might want to add it to your /etc/profile)

Test environment
----------------

Serveur
-------

RAM 1Go
CPU 1CPU 2 GHz
DD 20Go
Debian 4.0 64bits

Client
------

RAM 512Mo
CPU 1CPU 2 GHz
DD 9Go
Centos 5.2 32bits

Graphs reading
--------------

Horizontal data are read as following:
- In `ab` context, it represents concurrency level.
- In `httperf` context, it represents sent request rate.

Application launch
------------------

1) admin
nagare-admin serve -c <PUBLISHER_CONF> admin

2) nagare_pong
nagare-admin serve -c <PUBLISHER_CONF> nagare_pong

PS:
First create application with nagare-admin create-app nagare_pong and replace app.py with the foollowing file:
perf_graphs/pongs/nagare.py

##############################

3) test_concurrence
python perf_graphs/pongs/concurrence.py

PS:
python is the one used to install nagare

4) test_syncless
python perf_graphs/pongs/syncless.py

Data folders description
------------------------

ab_high
=======

Result of ab_high.sh against nagare_admin and nagare_pong with different publishers configuration

ab_high_pong
============

Result of ab_high.sh against nagare_pong with different publishers configuration and simple pong app for each publisher

ab_low
======

Result of ab_low.sh against nagare_admin and nagare_pong with different publishers configuration

ab_low_pong
===========

Result of ab_low.sh against nagare_pong with different publishers configuration and simple pong app for each publisher

httperf_high
============

Result of autobench (autobench_high.conf) against nagare_admin and nagare_pong with different publishers configuration

httperf_high_pong
=================

Result of autobench (autobench_high.conf) against nagare_pong with different publishers configuration and simple pong app for each publisher

httperf_low
===========

Result of autobench (autobench_low.conf) against nagare_admin and nagare_pong with different publishers configuration

httperf_low_pong
================

Result of autobench (autobench_low.conf) against nagare_pong with different publishers configuration and simple pong app for each publisher

Problem with syncless
---------------------

1) When a drop occurs on syncless graph the following error show in console:

Traceback (most recent call last):
  File "/home/nagare-test/stackless_venv/bin/nagare-admin", line 8, in <module>
    load_entry_point('nagare==0.3.0', 'console_scripts', 'nagare-admin')()
  File "/home/nagare-test/nagare_core/nagare/admin/main.py", line 73, in main
    command.run(parser, options, args)  # Run the command
  File "/home/nagare-test/nagare_core/nagare/admin/serve.py", line 284, in run
    publisher.serve(options.conf, pconf['publisher'], parser.error)
  File "/home/nagare-test/nagare_core/nagare/publishers/syncless_publisher.py", line 53, in serve
    wsgi.RunHttpServer(self.urls, (host, port))
  File "/home/nagare-test/stackless_venv/lib/python2.6/site-packages/syncless/wsgi.py", line 919, in RunHttpServer
    stackless.schedule_remove()
  File "/home/nagare-test/stackless_venv/lib/python2.6/site-packages/syncless/wsgi.py", line 244, in WsgiWorker
    req_new = sock.recv(4096)
  File "nbevent.pxi", line 1525, in coio.nbsocket.recv
  File "nbevent.pxi", line 1523, in coio.recv
socket.error: [Errno 104] Connection reset by peer

2) When using ab_high against nagare_pong + syncless server runs out of memory/kernel and kills everything

TODO
----

Add tests with fastcgi (apache + flup) and thread server (standalone).
