# -*- coding:utf-8 -*-
VERSION = '0.0.2'

from setuptools import setup, find_packages

setup(
    name='perf_graphs',
    version=VERSION,
    author=u'Hervé Coatanhay',
    author_email='herve.coatanhay@net-ng.com',
    description='',
    license='',
    keywords='',
    url='',
    packages=find_packages(),
    include_package_data=True,
    package_data={'': ['*.cfg']},
    zip_safe=False,
    install_requires=('nagare[i18n]', 'nagare-ui', 'nagare-jquery', 'nagare-session-redis', 'httperfpy', 'pyzmq',
                      'cloud', 'pygal', 'supervisor', 'paramiko'),
    entry_points="""
      [nagare.applications]
      perf_graphs = perf_graphs.app:app

      [perf_graphs.widgets]
      chart = perf_graphs.services.charts:Charts
      admin = perf_graphs.services.admin:Admin

      [perf_graphs.services]
      csv = perf_graphs.services.csv_test_storage:CSVTestStorage
      httperf = perf_graphs.services.httperf_test_runner:HTTPerfTestRunner
      autobench_suite = perf_graphs.services.autobench_test_suite:AutobenchTestSuite

      [console_scripts]
      perf-taskqueue = perf_graphs.services.tasks.task_queue:Worker.run
      """
)
